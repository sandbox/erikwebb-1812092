<?php

/**
 * @file
 * Install functions for the Drupal backend for Benchmark module.
 */

/**
 * Implements hook_schema().
 */
function benchmark_backend_drupal_schema() {
  $schema = array();

  $schema['benchmark'] = array(
    // Specification for table "benchmark"
    'description' => 'The base table for benchmark data when stored by Drupal.',
    'fields' => array(
      'mid' => array(
        'description' => 'The primary identifier of this table. This value will never be used, but must be implemented to have a primary key for this table.',
        'type' => 'serial', 
        'unsigned' => TRUE, 
        'not null' => TRUE,
      ),
      'module' => array(
        'description' => 'The source module of this metric.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ), 
      'token' => array(
        'description' => 'The specific token of this metric.',
        'type' => 'varchar',
        'length' => 64, 
        'not null' => TRUE,
        'default' => '',
      ),
      'value' => array(
        'description' => 'The measured value of this metric.',
        'type' => 'float',
        'not null' => TRUE,
      ),
      'data' => array(
        'type' => 'blob',
        'not null' => FALSE,
        'size' => 'big',
        'serialize' => TRUE,
        'description' => 'Additional information provided with the metric.',
      ),
    ), 
    'indexes' => array(
      'module' => array('module'),
      'module_token' => array('module', 'token'),
    ),
    'primary key' => array('mid'),
  );

  return $schema;
}
