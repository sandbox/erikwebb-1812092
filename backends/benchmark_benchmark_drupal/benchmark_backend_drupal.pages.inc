<?php

/**
 * Reports for the Drupal backend for Benchmark module.
 */

/**
 * Display overview of collected metrics.
 */
function benchmark_backend_drupal_report() {
  $header = array();
  $rows = array();
  $output = '';

  $header = array(
    array('data' => t('Module'), 'field' => 'module', 'sort' => 'ASC'),
    array('data' => t('Token'), 'field' => 'token', 'sort' => 'ASC'),
    array('data' => t('Count'), 'field' => 'count'),
    array('data' => t('Avg. Value'), 'field' => 'value_avg'),
    array('data' => t('Min. Value'), 'field' => 'value_min'),
    array('data' => t('Max. Value'), 'field' => 'value_max'),
    array('data' => t('Std. Deviation'), 'field' => 'value_stddev'),
  );

  $query = db_select('benchmark', 'b');
  $query->fields('b', array('module', 'token'));

  // Add calculated fields.
  $query->addExpression('COUNT(token)', 'count');
  $query->addExpression('AVG(value)', 'value_avg');
  $query->addExpression('MIN(value)', 'value_min');
  $query->addExpression('MAX(value)', 'value_max');
  $query->addExpression('STDDEV_POP(value)', 'value_stddev');

  $query->groupBy('module')
    ->groupBy('token')
    ->extend('TableSort')
    ->orderByHeader($header)
    ->orderBy('module', 'ASC')
    ->orderBy('token', 'ASC')
    ->extend('PagerDefault')
    ->limit(25);

  $result = $query->execute();
  foreach ($result as $row) {
    $rows[] = array(
      $row->module,
      $row->token,
      $row->count,
      round($row->value_avg, 2),
      round($row->value_min, 2),
      round($row->value_max, 2),
      round($row->value_stddev, 2),
    );
  }

  $output .= theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'empty' => t('No metrics available.'),
    'attributes' => array('id' => 'benchmark'),
  ));
  $output .= theme('pager');

  return $output;
}
