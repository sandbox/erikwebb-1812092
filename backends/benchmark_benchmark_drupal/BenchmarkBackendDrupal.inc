<?php

/**
 * @file
 * Definition of BenchmarkBackendDrupal.
 */

class BenchmarkBackendDrupal {

  public function record($module, $token, $value, $options = array()) {
    db_insert('benchmark')
      ->fields(array(
        'module' => $module,
        'token' => is_array($token) ? implode('.', $token) : $token,
        'value' => $value,
      ))
      ->execute();
  }

}
