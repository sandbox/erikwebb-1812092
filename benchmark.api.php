<?php

/**
 * @file
 * API documentation for Benchmark module.
 */

/**
 * Define available backends for the Benchmark module.
 *
 * This hook can supply permissions that the module defines, so that they
 * can be selected on the user permissions page and used to grant or restrict
 * access to actions the module performs.
 *
 * @return
 * An array whose keys are backend machines names and whose corresponding values
 * are arrays containing the following key-value pairs:
 * - title: The human-readable name of the backend, to be shown on the
 * Benchmark administration page. This should be wrapped in the t()
 * function so it can be translated.
 * - class: (optional) The name of the class that will be instantiated in order
 * to record the metric.
 */
function hook_benchmark_backends() {
 return array(
   'pen_paper' => array(
     'title' => t('Pen & Paper'),
     'class' => 'BenchmarkBackendDrupal',
   ),
 );
}
