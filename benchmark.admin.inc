<?php

/**
 * @file
 * Administrative forms for Benchmark module.
 */

/**
 * Administration form for configuration.
 */
function benchmark_admin_form() {
  $form = array();
  $backends = array();

  foreach (benchmark_get_backends() as $machine_name => $backend) {
    $backends[$machine_name] = $backend['title'];
  }

  $form['benchmark_ignore_admin'] = array(
    '#title' => t('Disable benchmarking on admin pages'),
    '#type' => 'checkbox',
    '#description' => t('Enable this option to prevent admin pages from storing any benchmark metrics.'),
    '#default_value' => variable_get('benchmark_ignore_admin', TRUE),
  );

  $form['benchmark_ignore_cli'] = array(
    '#title' => t('Disable benchmarking of CLI requests (including Drush)'),
    '#type' => 'checkbox',
    '#description' => t('Enable this option to prevent comamnd-line requests from storing any benchmark metrics.'),
    '#default_value' => variable_get('benchmark_ignore_cli', TRUE),
  );

  $form['benchmark_exclude_tokens'] = array(
    '#title' => t('Exclude tokens'),
    '#type' => 'textarea',
    '#description' => t('Ignore benchmarks when any of the exclusions above match <em>any part</em> of the token. Put each exclusion on a separate line. You can use the * character (asterisk) as a wildcard.'),
    '#default_value' => variable_get('benchmark_exclude_tokens', 'admin/*'),
  );

  return system_settings_form($form);
}
