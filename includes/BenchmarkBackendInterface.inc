<?php

/**
 * @file
 * Definition of BenchmarkBackendInterface.
 */

interface BenchmarkBackendInterface {

  /**
   * Record the generated metric in the current backend.
   *
   * @param $module
   *   The source module of this metric.
   * @param $token
   *   The specific token of this metric.
   * @param $value
   *   The measured value of this metric.
   * @param $options
   *   (optional) A set of additional values relevant to this metric. Backends
   *   may individually choose whether these values will be used.
   */
  public function record($module, $token, $value, $options = array());

}
